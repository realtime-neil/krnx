#!/usr/bin/make -f

include /usr/share/dpkg/default.mk

export DH_VERBOSE = 1

# From the upstream version (assumed to be a classic dotted triplet) discover
# the major version number. Use this when we construct the first symbolic link
# to the shared object.
DEB_VERSION_UPSTREAM_MAJOR := $(word 1, $(subst ., ,$(DEB_VERSION_UPSTREAM)))

# One of the problems with the 'khi_robot_control' subdirectory is that
# Kawasaki has only supplied five shared objects. Each of the five '*.so' files
# has a non-standard architecture designation. We need to map the
# DEB_TARGET_ARCH to this non-standard KRNX_ARCH such that (if the Debian
# target architecture is supported) we know which shared object to use.
ifeq ($(DEB_TARGET_ARCH),arm64)
	KRNX_ARCH = aarch64
endif
ifeq ($(DEB_TARGET_ARCH),armel)
	KRNX_ARCH = armel
endif
ifeq ($(DEB_TARGET_ARCH),armhf)
	KRNX_ARCH = armhf
endif
ifeq ($(DEB_TARGET_ARCH),i386)
	KRNX_ARCH = i386
endif
ifeq ($(DEB_TARGET_ARCH),amd64)
	KRNX_ARCH = x86_64
endif
ifndef KRNX_ARCH
	$(error "unsupported DEB_TARGET_ARCH: $(DEB_TARGET_ARCH)")
endif

%:
	dh $@ -v --parallel

# We're not "building" anything.
override_dh_auto_build:
	echo NOOP $@

# The original shared object has no embedded SONAME. We use 'patchelf' to
# remedy this.
#
# The original shared object has no SOVERSION-specific symbolic links. We use
# 'ln' to remedy this.
override_dh_auto_install:
	mkdir -vp \
		debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)
	cp -va \
		khi_robot_control/lib/libkrnx-$(KRNX_ARCH).so \
		debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libkrnx.so.$(DEB_VERSION_UPSTREAM)
	patchelf \
		--set-soname libkrnx.so.$(DEB_VERSION_UPSTREAM_MAJOR) \
		debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libkrnx.so.$(DEB_VERSION_UPSTREAM)
	ln -vsnf \
		libkrnx.so.$(DEB_VERSION_UPSTREAM) \
		debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libkrnx.so.$(DEB_VERSION_UPSTREAM_MAJOR)
	ln -vsnf \
		libkrnx.so.$(DEB_VERSION_UPSTREAM_MAJOR) \
		debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libkrnx.so
	mkdir -vp \
		debian/tmp/usr/include
	cp -va \
		khi_robot_control/include/krnx.h \
		debian/tmp/usr/include

# Don't let dwz see the shared object we patched. Otherwise, this happens:
#
#   dh_dwz -a -O-v -O--parallel
#
#   dwz -- debian/libkrnx1/usr/lib/x86_64-linux-gnu/libkrnx.so.1.3.0
#
#   dwz: Allocatable section in
#   debian/libkrnx1/usr/lib/x86_64-linux-gnu/libkrnx.so.1.3.0 after
#   non-allocatable ones
#
#   dh_dwz: error: dwz --
#   debian/libkrnx1/usr/lib/x86_64-linux-gnu/libkrnx.so.1.3.0 returned exit
#   code 1
#
#   dh_dwz: error: Aborting due to earlier error
override_dh_dwz:
	echo NOOP $@
